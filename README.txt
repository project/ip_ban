IP Ban Module
====================

Description
-----------

A large majority of all websites have content that's only relevant to a country
or region. This can be problematic because visitors from other countries or
regions may not exactly be visiting your site with your best interests in mind.

This module allows you configure the site to be "read only" or "complete ban"
for a visitor based on individual IP addresses or by country (using IP address
lookup).

For complete bans, you can redirect the user to any page you choose and/or
display an "error" message. There is nothing preventing you from entering an
external address as the page to redirect to.

"Read only" will allow you to disable blocks for visitors from a country or IP
address set to "Read only", and disallows access to all /user pages. If you also
need to disable forms, it is recommended you install the Read only mode module.

Combined with a sensible use of block visibility on the pages you redirect to
for blocks containing menus,  phone numbers, etc., you should be able to
effectively block out unwanted contacts.


Installation
------------

See https://www.drupal.org/docs/8/extending-drupal-8/installing-contributed-modules-find-import-enable-configure

Note that this module requires the ip2country module; see
https://www.drupal.org/project/ip2country

Author
------

Written by: Daniel Harris of Webdrips (http://webdrips.com and
https://www.drupal.org/u/webdrips)

If you use this module, find it useful, and want to send the author
a thank you note, then use the Feedback/Contact page at the URL above.
