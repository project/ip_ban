<?php

/**
 * @file
 * Module can "ban" or set site to "read only" by country or IP address.
 *
 * Permissions, administration menu item, form, form theme, form validation,
 * and code for handling read-only and banned IP addresses by country or
 * individually.
 */

use Drupal\block\Entity\Block;

/**
 * Implements hook_theme().
 */
function ip_ban_theme(): array {
  $themes = [
    'ip_ban_country_table' => [
      'render element' => 'ip_ban_table',
    ],
  ];
  return $themes;
}

/**
 * Implements hook_block_access($block).
 *
 * Todo: need to move the ip_ban_determine_action to a new location so it's not
 * called once for each block.
 */
function ip_ban_block_access(Block $block) {
  if (\Drupal::currentUser()->hasPermission('ignore ip_ban')) {
    // Take no action if the user has the "Bypass ban" permission.
    return;
  }

  $disabled_block_list = [];
  $disabled_blocks = \Drupal::config('ip_ban.settings')->get('ip_ban_disabled_blocks');
  if (!empty($disabled_blocks)) {
    $disabled_block_array = explode(PHP_EOL, $disabled_blocks);
    foreach ($disabled_block_array as $disabled_block) {
      $disabled_block_list[] = preg_replace('/\s+/', '', $disabled_block);
    }
  }
  if (!empty($disabled_block_list)) {
    if (in_array($block->id(), $disabled_block_list)) {
      unset($block);
    }
  }
}
