<?php

namespace Drupal\ip_ban;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Provides a HTTP middleware to implement IP based banning.
 */
class IpBanMiddleware implements HttpKernelInterface {

  /**
   * The decorated kernel.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected $httpKernel;

  /**
   * The IP Ban manager service instance.
   *
   * @var \Drupal\ip_ban\IpBanSetBanInterface
   */
  protected $ipBanManager;

  /**
   * Constructs a BanMiddleware object.
   *
   * @param \Symfony\Component\HttpKernel\HttpKernelInterface $http_kernel
   *   The decorated kernel.
   * @param \Drupal\ban\IpBanSetBanInterface $manager
   *   The IP Ban manager service instance.
   */
  public function __construct(HttpKernelInterface $http_kernel, IpBanSetBanInterface $manager) {
    $this->httpKernel = $http_kernel;
    $this->ipBanManager = $manager;
  }

  /**
   * {@inheritdoc}
   */
  public function handle(Request $request, $type = self::MAIN_REQUEST, $catch = TRUE): Response {
    $this->ipBanManager->ipBanSetValue();
    $this->ipBanManager->ipBanDetermineAction();
    return $this->httpKernel->handle($request, $type, $catch);
  }

}
