<?php

declare(strict_types = 1);

namespace Drupal\ip_ban;

use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\ip2country\Ip2CountryLookupInterface;
use Drupal\path_alias\AliasManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Implementation of the IP ban value manager.
 */
class IpBanSetBanManager implements IpBanSetBanInterface {

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  private $user;

  /**
   * The read only module configuration service instance.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  private $config;

  /**
   * The client IP address.
   *
   * @var string
   */
  private $clientIp;

  /**
   * The ip2country lookup service instance.
   *
   * @var \Drupal\ip2country\Ip2CountryLookupInterface
   */
  private $country_lookup;

  /**
   * The current path for the current request.
   *
   * @var \Drupal\Core\Path\CurrentPathStack
   */
  private $path;

  /**
   * The default path alias manager implementation.
   */
  private $path_alias;

  /**
   * Default Drupal messenger service instance.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  private $messenger;

  /**
   * The user's ban value: IP_BAN_NOBAN, IP_BAN_READONLY, or IP_BAN_BANNED.
   *
   * @var int
   */
  private $banvalue;

  /**
   * Default class constructor
   *
   * @param \Drupal\Core\Session\AccountProxyInterface $user
   *   Current user.
   * @param \Drupal\Core\Config\ImmutableConfig $config
   *   The read only module configuration service instance.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request
   *   The currently active request object.
   * @param Drupal\ip2country\Ip2CountryLookupInterface $country_lookup
   *   The ip2country lookup service instance.
   * @param \Drupal\Core\Path\CurrentPathStack $path
   *   The current path for the current request.
   * @param \Drupal\path_alias\AliasManagerInterface $path_alias
   *   The default path alias manager implementation.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Default Drupal messenger service instance.
   */
  public function __construct(AccountProxyInterface $user, ImmutableConfig $config, RequestStack $request, Ip2CountryLookupInterface $country_lookup, CurrentPathStack $path, AliasManagerInterface $path_alias, MessengerInterface $messenger) {
    $this->user = $user;
    $this->config = $config;
    $current = $request->getCurrentRequest();
    $this->clientIp = $current === NULL ? $_SERVER['REMOTE_ADDR'] : $current->getClientIp();
    $this->country_lookup = $country_lookup;
    $this->path = $path;
    $this->path_alias = $path_alias;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public function ipBanSetValue(): void {
    // If code is being run from drush, we don't want to take any action.
    if ((PHP_SAPI === "cli") && function_exists('drush_main')) {
      return;
    }
    // If user has permission to bypass ban, set to IP_BAN_NOBAN and return.
    if ($this->user->hasPermission('ignore ip_ban')) {
      $this->banvalue = self::IP_BAN_NOBAN;
      return;
    }
    $test_ip = $this->config->get('ip_ban_test_ip');
    // Grab the test IP address or the user's real address.
    $ip = empty($test_ip) ? $this->clientIp : $test_ip;
    $country_code = $this->country_lookup->getCountry($ip);
    // Determine if the current user is banned or read only.
    // Individual IP complete ban trumps individual read only IP, both of which
    // trump a country setting.
    if (!isset($this->banvalue)) {
      $banvalue = (int) $this->config->get('ip_ban_' . $country_code);
      $this->banvalue = $banvalue;
      // Check the read-only IP list.
      $readonly_ips = $this->config->get('ip_ban_readonly_ips');
      if (!empty($readonly_ips)) {
        $ip_readonly_array = explode(PHP_EOL, $readonly_ips);
        if (in_array($ip, $ip_readonly_array)) {
          $this->$banvalue = self::IP_BAN_READONLY;
        }
      }
      // Check the complete ban list.
      $banned_ips = $this->config->get('ip_ban_additional_ips');
      if (!empty($banned_ips)) {
        $ip_ban_array = explode(PHP_EOL, $banned_ips);
        if (in_array($ip, $ip_ban_array)) {
          $this->banvalue = self::IP_BAN_BANNED;
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getBanValue(): int {
    return $this->banvalue;
  }

  /**
   * {@inheritdoc}
   */
  public function ipBanDetermineAction() {
    if ($this->banvalue === self::IP_BAN_READONLY) {
      $uri = $this->path->getPath();
      if (($uri == 'user') || strpos($uri, 'user/') !== FALSE) {
        $path = $this->config->get('ip_ban_readonly_path');
        $response = new RedirectResponse($path);
        $response->send();
        exit;
      }
    }
    if ($this->banvalue === self::IP_BAN_BANNED) {
      // Always allow access to the banned page.
      $complete_ban_path = $this->config->get('ip_ban_completeban_path');
      if (!empty($complete_ban_path) && $this->path->getPath() !== $this->path_alias->getPathByAlias($complete_ban_path)) {
        $response = new RedirectResponse($complete_ban_path);
        $response->send();
      }
      else {
        $this->messenger->addError($this->config->get('ip_ban_completeban'));
      }
    }
  }

}
