<?php

declare(strict_types = 1);

namespace Drupal\ip_ban\EventSubscriber;

use Drupal\ip_ban\IpBanSetBanInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * The IP Ban request events subscriber implementation.
 */
class IpBanSubscriber implements EventSubscriberInterface {

  /**
   * The IP ban manager service instance.
   *
   * @var \Drupal\ip_ban\IpBanSetBanInterface
   */
  private $ipBanManager;

  /**
   * Default class constructor.
   *
   * @param \Drupal\ip_ban\IpBanSetBanInterface $manager
   *   The IP Ban manager.
   */
  public function __construct(IpBanSetBanInterface $manager) {
    $this->ipBanManager = $manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::REQUEST => ['hookInit', 255],
    ];
  }

  /**
   * Sets the ban value and action for authenticated users.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The event to process.
   */
  public function hookInit(RequestEvent $event) {
    $this->ipBanManager->ipBanSetValue();
    $this->ipBanManager->ipBanDetermineAction();
  }

}
